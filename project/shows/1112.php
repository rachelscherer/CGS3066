<!DOCTYPE html>
<html>
<head>
	<title>2011-2012 season</title>
	<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
	<?php
		include '../mysql_info.php'; 
    ?>
    <div class="top">
	<div class="topbar">
		<div class="topcontentpad">
        <a href="../index.php"><img src="../logo.png" alt="Working logo" /></a>
		<div class="dropdown">
			<button class="dropbtn">Become Involved</button>
			<div class="dropdown-content">
				<a href="../becomeinvolved/volunteer.php">Volunteer</a> <a href ="../becomeinvolved/member.php">Become a Member</a><a href="../becomeinvolved/auditions.php">Audition</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">Shows</button>
			<div class="dropdown-content">
				<a href="../shows/current.php">Current Season</a> <hr /> <a href="../shows/0708.php">2007-2008 Season</a> <a href="../shows/0809.php">2008-2009 Season</a> <a href="../shows/0910.php">2009-2010 Season</a> <a href="../shows/1011.php">2010-2011 Season</a> <a href="#">2011-2012 Season</a> <a href="../shows/1213.php">2012-2013 Season</a> <a href="../shows/1314.php">2013-2014 Season</a> <a href="../shows/1415.php">2014-2015 Season</a> <a href="../shows/1516.php">2015-2016 Season</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">News</button>
			<div class="dropdown-content">
				<a href="../news/articles.php">Articles</a> <a href="../news/email.php">Email List</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">About Us</button>
			<div class="dropdown-content">
				<a href="../about/mission.php">Mission</a> <a href="../about/history.php">History</a> <a href="../about/staff.php">Staff</a> <a href ="../about/contact.php">Contact Us</a>
			</div>
		</div>
		<div class="dropdown">
			<a href="../login.php"><button class="dropbtn">Edit</button></a>
		</div>
		</div>
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<h1 class="logo">Daytona Beach Youth Theatre</h1>
	</div>
	</div>
	<div class="container1">
		<div class="content">
			<span style="text-align: center; font-size: 1.65em"><i><h1>2011 - 2012 season</h1></i></span>
		</div>
	</div>
	<div class="container2show">
		<div class="content">
			<span style="text-align: center"><p>Here are the shows of this season:</p></span>
			<div class="show1"><img src="posters/show1.png" alt="Show 1"/></div>
			<div class="show3"><img src="posters/show3.png" alt="Show 2"/></div>
			<div class="show2"><img src="posters/show2.png" alt="Show 3"/></div>
			<div class="show1"><p>August 13-15, 2011</p></div>
			<div class="show3"><p>April 14-16, 2012</p></div>
			<div class="show2"><p>November 12-14, 2011</p></div>
		</div>
	</div>
	<div class="footer">
		<div class="footerleft">
			<?php
			if ($_SESSION['login'] == "1") {
				echo "<p>You are currently logged in.</p>";
			} else{
				echo "<p>You are currently logged out</p>";
			}
			?>
			<p>This is a made up organization. None of the content on this page is real.</p>
		</div>
		<div class="footerright">
			<p><b>Daytona Beach Youth Theatre</b></p>
			<p>500 Avenue Ave.</p>
			<p>Daytona Beach, Florida 32334</p>
			<p>555-555-5555</p>
		</div>
	</div>
</body>
</html>