<html>
<head>
	<title>Join our mailing list!</title>
	<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
    <div class="top">
	<div class="topbar">
		<div class="topcontentpad">
        <a href="../index.php"><img src="../logo.png" alt="Working logo" /></a>
		<div class="dropdown">
			<button class="dropbtn">Become Involved</button>
			<div class="dropdown-content">
				<a href="../becomeinvolved/volunteer.php">Volunteer</a> <a href ="../becomeinvolved/member.php">Become a Member</a><a href="../becomeinvolved/auditions.php">Audition</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">Shows</button>
			<div class="dropdown-content">
				<a href="../shows/current.php">Current Season</a> <hr /> <a href="../shows/0708.php">2007-2008 Season</a> <a href="../shows/0809.php">2008-2009 Season</a> <a href="../shows/0910.php">2009-2010 Season</a> <a href="../shows/1011.php">2010-2011 Season</a> <a href="../shows/1112.php">2011-2012 Season</a> <a href="../shows/1213.php">2012-2013 Season</a> <a href="../shows/1314.php">2013-2014 Season</a> <a href="../shows/1415.php">2014-2015 Season</a> <a href="../shows/1516.php">2015-2016 Season</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">News</button>
			<div class="dropdown-content">
				<a href="../news/articles.php">Articles</a> <a href="#">Email List</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">About Us</button>
			<div class="dropdown-content">
				<a href="../about/mission.php">Mission</a> <a href="../about/history.php">History</a> <a href="../about/staff.php">Staff</a> <a href ="../about/contact.php">Contact Us</a>
			</div>
		</div>
		<div class="dropdown">
			<a href="../login.php"><button class="dropbtn">Edit</button></a>
		</div>
		</div>
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<h1 class="logo">Daytona Beach Youth Theatre</h1>
	</div>
	</div>
	<div class="container2email">
		<div class="content">
			<?php
			include '../mysql_info.php';
            $connect = mysqli_connect($servername, $username, $password, $dbname); 
			
			if($_SERVER["REQUEST_METHOD"] == "POST"){
				if(mysqli_connect_errno()){
						echo "Failed to connect to MySQL: " . mysqli_connect_error();
				} else{
					$email = $_POST['email'];
					$query = 'INSERT INTO emails VALUES ("'.$email.'");';
	
					if($connect->query($query) === true){
						$message = "Success! ".$email. " is now part of our mailing list!";
					} else{
						$message = "Error! " .$email. " already exists in the database.";
					}
					mysqli_close($connect);
				}
			}
			

			?>
			<p>Enter your email address below to become part of our amazing mailing list!</p>
			<form method = "POST" name="myForm" onsubmit="return checkEmail();" action="">
			<input type="text" name="email">
			<input type="submit" value="Submit">
			</form><br /><span id="emailMsg"><?php echo $message; ?></span>
		</div>
	</div>
	<div class="footer">
		<div class="footerleft">
			<?php
			if ($_SESSION['login'] == "1") {
				echo "<p>You are currently logged in.</p>";
			} else{
				echo "<p>You are currently logged out</p>";
			}
			?>
			<p>This is a made up organization. None of the content on this page is real.</p>
		</div>
		<div class="footerright">
			<p><b>Daytona Beach Youth Theatre</b></p>
			<p>500 Avenue Ave.</p>
			<p>Daytona Beach, Florida 32334</p>
			<p>555-555-5555</p>
		</div>
	</div>
	<script>
		function checkEmail() {
            var email = document.forms["myForm"]["email"].value;
			var emailCheck = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			
			if (email === ""){
				document.getElementById("emailMsg").innerHTML = "Error! Email cannot be blank";
				return false;
			} else if (emailCheck.test(email) === false){
				document.getElementById("emailMsg").innerHTML = "Error! Email must follow this syntax: id@domain";
				return false;
			} else{
				return true;
			}
        }
	</script>
</body>
</html>