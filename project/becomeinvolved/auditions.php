<!DOCTYPE html>
<html>
<head>
	<title>Audition</title>
	<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
	<?php
		include '../mysql_info.php'; 
    ?>
    <div class="top">
	<div class="topbar">
		<div class="topcontentpad">
        <a href="../index.php"><img src="../logo.png" alt="Working logo" /></a>
		<div class="dropdown">
			<button class="dropbtn">Become Involved</button>
			<div class="dropdown-content">
				<a href="../becomeinvolved/volunteer.php">Volunteer</a> <a href ="../becomeinvolved/member.php">Become a Member</a><a href="#">Audition</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">Shows</button>
			<div class="dropdown-content">
				<a href="../shows/current.php">Current Season</a> <hr /> <a href="../shows/0708.php">2007-2008 Season</a> <a href="../shows/0809.php">2008-2009 Season</a> <a href="../shows/0910.php">2009-2010 Season</a> <a href="../shows/1011.php">2010-2011 Season</a> <a href="../shows/1112.php">2011-2012 Season</a> <a href="../shows/1213.php">2012-2013 Season</a> <a href="../shows/1314.php">2013-2014 Season</a> <a href="../shows/1415.php">2014-2015 Season</a> <a href="../shows/1516.php">2015-2016 Season</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">News</button>
			<div class="dropdown-content">
				<a href="../news/articles.php">Articles</a> <a href="../news/email.php">Email List</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">About Us</button>
			<div class="dropdown-content">
				<a href="../about/mission.php">Mission</a> <a href="../about/history.php">History</a> <a href="../about/staff.php">Staff</a> <a href ="../about/contact.php">Contact Us</a>
			</div>
		</div>
		<div class="dropdown">
			<a href="../login.php"><button class="dropbtn">Edit</button></a>
		</div>
		</div>
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<h1 class="logo">Daytona Beach Youth Theatre</h1>
	</div>
	</div>
	<div class="container1">
		<div class="content">
			<span style="text-align: center; font-size: 1.65em"><i><h1>Auditions</h1></i></span>
		</div>
	</div>
	<div class="container2auditions">
		<div class="content">
			<img src="frogboy.jpeg" alt = "Audition today!" style = "float: left; margin-right: 15px; width: 300px;"/>
			<p><span style="font-size: 1.5em"><b>Daytona Beach Youth Theatre Auditions </b></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce gravida purus ut justo posuere euismod. Nullam pharetra libero arcu, ut facilisis odio vulputate sed. Duis imperdiet odio suscipit, egestas felis vitae, accumsan lacus. Nunc eget aliquam neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec rhoncus, erat et hendrerit bibendum, dui magna posuere tortor, eget laoreet velit nunc posuere ex. Curabitur sodales turpis id justo rhoncus, nec dignissim leo tempus. Nam at urna non felis lobortis hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ex nulla, dictum vel elementum eget, consequat non ex. Curabitur nec ligula risus. Vivamus bibendum pretium orci, ac ultricies ligula feugiat vel. Duis dictum nunc magna, ut mollis quam posuere eu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sodales elit eget tempus lobortis. Praesent pharetra orci ut est ultrices tristique.</p>
			<p>For more information about upcoming auditions, please check below:</p>	
		</div>
	</div>
	<div class="container3auditions">
		<div class="content">
			<br />
			<img src="../shows/posters/lorax.png" alt="Thoroughly Modern Millie, Jr." style="float: left; margin-right: 15px; width: 170px;"/>
			<h2>The Lorax</h2>
			<p>Performance dates: April 7-9, 2017</p><br />
			<p>Book by: David Greig</p>
			<p>Music by: Charlie Fink</p>
			<p>Audition packet: Coming soon!</p><br />
			<p>For more information about our upcoming productions, please click <a href="../shows/current.php" target="_blank">Here.</a></p>
		</div>
	</div>
	<div class="footer">
		<div class="footerleft">
			<?php
			if ($_SESSION['login'] == "1") {
				echo "<p>You are currently logged in.</p>";
			} else{
				echo "<p>You are currently logged out</p>";
			}
			?>
			<p>This is a made up organization. None of the content on this page is real.</p>
		</div>
		<div class="footerright">
			<p><b>Daytona Beach Youth Theatre</b></p>
			<p>500 Avenue Ave.</p>
			<p>Daytona Beach, Florida 32334</p>
			<p>555-555-5555</p>
		</div>
	</div>
</body>
</html>