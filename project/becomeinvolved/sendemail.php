<html>
 <head>
  <meta http-equiv="refresh" content="4;url=../becomeinvolved/volunteer.php" />
 </head>
<body>

<?php
    include '../mysql_info.php';
    $connect = mysqli_connect($servername, $username, $password, $dbname);
    
    if(mysqli_connect_errno()){
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
    } else{
            $name = $_POST["name"];
            $email = $_POST["email"];
            $phone = $_POST["phone"];
            $other = $_POST["other"];
            
            $checkedstring = "Checked shows: ";
            
            if(!empty($_POST["balloonacy"])){
                $checkedstring .= "Balloonacy, ";
            }
            if(!empty($_POST["lorax"])){
                $checkedstring .= "Lorax, ";
            }
            if(!empty($_POST["millie"])){
                $checkedstring .= "Millie";
            }
            
            $checkedstring .= ". --- Checked jobs: ";
            
            if(!empty($_POST["tickets"])){
                $checkedstring .= "Ticket scanning, ";
            }
            if(!empty($_POST["willcall"])){
                $checkedstring .= "Will Call, ";
            }
            if(!empty($_POST["fundraising"])){
                $checkedstring .= "Fundraising, ";
            }
            if(!empty($_POST["ushering"])){
                $checkedstring .= "Ushering, ";
            }
            if(!empty($_POST["orchestra"])){
                $checkedstring .= "Orchestra, ";
            }
            if(!empty($_POST["othercheck"])){
                $checkedstring .= "Other";
            }
            
            $checkedstring .= ". --- Comment: " . $other;
            
            
            $from = "From: daytonabeachyouththeatre.com";
            $to = "daytonabeachyouththeatre@gmail.com";
            $subject = "Volunteer - Comment from " . $name . "";
            $message = "Message from " . $name . ". --- Email: " . $email . ". --- Phone: " . $phone . ". " . $checkedstring;
            mail($to,$subject,$message,$from);
           
            echo "Message sent! Refreshing page...";
    }
?>
</body>
</html>