<!DOCTYPE html>
<html>
<head>
	<title>Daytona Beach Youth Theatre Homepage</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
    <div class="top">
		<div class="topbar">
			<div class="topcontentpad">
				<a href="#"><img src="logo.png" alt="Working logo" /></a>
				<div class="dropdown">
					<button class="dropbtn">Become Involved</button>
					<div class="dropdown-content">
						<a href="becomeinvolved/volunteer.php">Volunteer</a> <a href ="becomeinvolved/member.php">Become a Member</a><a href="becomeinvolved/auditions.php">Audition</a>
					</div>
				</div>
				<div class="dropdown">
					<button class="dropbtn">Shows</button>
					<div class="dropdown-content">
						<a href="shows/current.php">Current Season</a> <hr /> <a href="shows/0708.php">2007-2008 Season</a> <a href="shows/0809.php">2008-2009 Season</a> <a href="shows/0910.php">2009-2010 Season</a> <a href="shows/1011.php">2010-2011 Season</a> <a href="shows/1112.php">2011-2012 Season</a> <a href="shows/1213.php">2012-2013 Season</a> <a href="shows/1314.php">2013-2014 Season</a> <a href="shows/1415.php">2014-2015 Season</a> <a href="shows/1516.php">2015-2016 Season</a>
					</div>
				</div>
				<div class="dropdown">
					<button class="dropbtn">News</button>
					<div class="dropdown-content">
						<a href="news/articles.php">Articles</a> <a href="news/email.php">Email List</a>
					</div>
				</div>
				<div class="dropdown">
					<button class="dropbtn">About Us</button>
					<div class="dropdown-content">
						<a href="about/mission.php">Mission</a> <a href="about/history.php">History</a> <a href="about/staff.php">Staff</a> <a href ="about/contact.php">Contact Us</a>
					</div>
				</div>
				<div class="dropdown">
					<a href="login.php"><button class="dropbtn">Edit</button></a>
				</div>
			</div>
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<h1 class="logo">Daytona Beach Youth Theatre</h1>
		</div>
	</div>
	<div class="container1">
		<div class="content">
			<?php
			include 'mysql_info.php';
            $connect = mysqli_connect($servername, $username, $password, $dbname); 
			
			if(mysqli_connect_errno()){
					echo "Failed to connect to MySQL: " . mysqli_connect_error();
			} else{
					$query = "SELECT * FROM splash WHERE num=1";
					$result = mysqli_query($connect, $query) or die(mysqli_error());
					$row = mysqli_fetch_assoc($result);
					echo "<b><h1>".$row['title']."</h1></b>";
					echo "<i><h4>".$row['description']."</h4></i>";
			}
			

			?>

		</div>
	</div>
	<div class="container2">
		<div class="content">
			<i><h1>In the news...</h1></i>
			<?php
			include 'mysql_info.php';
            $connect = mysqli_connect($servername, $username, $password, $dbname); 
			
			if(mysqli_connect_errno()){
					echo "Failed to connect to MySQL: " . mysqli_connect_error();
			} else{
					$query = "SELECT * FROM articles";
					$result = mysqli_query($connect, $query) or die(mysqli_error());
					$countresult = $result->num_rows;
					$query = "SELECT * FROM articles WHERE articleID=$countresult";
					$result = mysqli_query($connect, $query) or die(mysqli_error());
					$row = mysqli_fetch_assoc($result);
					echo "<i><h3>".$row['title']."</h3></i>";
					echo "<p>".$row['body']."</p>";
			}

			?>
		</div>
	</div>
	<div class="container3">
		<div class="content">
			<b><h1>Become a member!</h1></b>
			<h4>This will feature a blurb about how it's important to become a member and support this organization</h4>
			<p>Lorem ipsum dolor sit amet, ridens facete perfecto ne pri, vix saepe intellegebat ne. Vim cu augue veritus dolores, illum lucilius eloquentiam cu sed. Alia augue philosophia mei ea. Vim at diam quaestio, agam democritum quo ad. Ea his natum mentitum omittantur, ad nam numquam euismod feugiat. Mei feugiat voluptaria ad, in nam omnis omnesque oporteat, saperet abhorreant vim ad.</p>
			<a href="becomeinvolved/member.php"><img src="becomemember.jpg" alt = "Become a member!" /></a>
			<p>Eam ex noster interesset, cu quo elit dolor veniam. Ullum etiam timeam mei id. Mei cu vide suavitate, paulo ubique sit ne, his prodesset eloquentiam id. Est ut zril apeirian, veri omnes dicam quo in, ad his viris soluta. An sea dolor utamur gloriatur. Ne hinc honestatis mea, vim justo nullam ex.</p>
			<p>Porro soluta dolores eu mea, vix soleat partiendo ea. Vim et euismod vituperata philosophia. Ex eos ferri accusamus molestiae, vix ut quis hendrerit. Eius soleat ea duo, ius scripta complectitur necessitatibus ad. Pro dicat percipit iudicabit et, ea pro error fabulas alienum. Cum ex persius vocibus vivendum, erant menandri cu sea.</p>
		</div>
	</div>
	<div class="container4">
		<div class="content">
			<b><h1>Classes</h1></b>
			<h4>This will feature information about the upcoming classes.</h4>
			<p>Lorem ipsum dolor sit amet, ridens facete perfecto ne pri, vix saepe intellegebat ne. Vim cu augue veritus dolores, illum lucilius eloquentiam cu sed. Alia augue philosophia mei ea. Vim at diam quaestio, agam democritum quo ad. Ea his natum mentitum omittantur, ad nam numquam euismod feugiat. Mei feugiat voluptaria ad, in nam omnis omnesque oporteat, saperet abhorreant vim ad.</p>
			<img src="classes.jpg" alt="classes">
			<p>Eam ex noster interesset, cu quo elit dolor veniam. Ullum etiam timeam mei id. Mei cu vide suavitate, paulo ubique sit ne, his prodesset eloquentiam id. Est ut zril apeirian, veri omnes dicam quo in, ad his viris soluta. An sea dolor utamur gloriatur. Ne hinc honestatis mea, vim justo nullam ex.</p>
			<p>Porro soluta dolores eu mea, vix soleat partiendo ea. Vim et euismod vituperata philosophia. Ex eos ferri accusamus molestiae, vix ut quis hendrerit. Eius soleat ea duo, ius scripta complectitur necessitatibus ad. Pro dicat percipit iudicabit et, ea pro error fabulas alienum. Cum ex persius vocibus vivendum, erant menandri cu sea.</p>
		</div>		
	</div>
	<div class="footer">
		<div class="footerleft">
			<?php
			if ($_SESSION['login'] == "1") {
				echo "<p>You are currently logged in.</p>";
			} else{
				echo "<p>You are currently logged out</p>";
			}
			?>
			<p>This is a made up organization. None of the content on this page is real.</p>
		</div>
		<div class="footerright">
			<p><b>Daytona Beach Youth Theatre</b></p>
			<p>500 Avenue Ave.</p>
			<p>Daytona Beach, Florida 32334</p>
			<p>555-555-5555</p>
		</div>
	</div>
</body>
</html>