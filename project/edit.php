<!DOCTYPE html>
<html>
<head>
	<title>Edit</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
    <?php
	include 'mysql_info.php';
    if ($_SESSION['login'] == "") {
        header("location:login.php");
    }    
    ?>
    <div class="top">
	<div class="topbar">
		<div class="topcontentpad">
        <a href="index.php"><img src="logo.png" alt="Working logo" /></a>
		<div class="dropdown">
			<button class="dropbtn">Become Involved</button>
			<div class="dropdown-content">
				<a href="becomeinvolved/volunteer.php">Volunteer</a> <a href ="becomeinvolved/member.php">Become a Member</a><a href="becomeinvolved/auditions.php">Audition</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">Shows</button>
			<div class="dropdown-content">
				<a href="shows/current.php">Current Season</a> <hr /> <a href="shows/0708.php">2007-2008 Season</a> <a href="shows/0809.php">2008-2009 Season</a> <a href="shows/0910.php">2009-2010 Season</a> <a href="shows/1011.php">2010-2011 Season</a> <a href="shows/1112.php">2011-2012 Season</a> <a href="shows/1213.php">2012-2013 Season</a> <a href="shows/1314.php">2013-2014 Season</a> <a href="shows/1415.php">2014-2015 Season</a> <a href="shows/1516.php">2015-2016 Season</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">News</button>
			<div class="dropdown-content">
				<a href="news/articles.php">Articles</a> <a href="news/email.php">Email List</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">About Us</button>
			<div class="dropdown-content">
				<a href="about/mission.php">Mission</a> <a href="about/history.php">History</a> <a href="about/staff.php">Staff</a> <a href ="about/contact.php">Contact Us</a>
			</div>
		</div>
		<div class="dropdown">
			<a href="#"><button class="dropbtn">Edit</button></a>
		</div>
		</div>
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<h1 class="logo">Daytona Beach Youth Theatre</h1>
	</div>
	</div>
	<div class="container1">
		<div class="content">
			<span style="text-align: center; font-size: 1.65em"><i><h1>Welcome to the backend!</h1></i></span>
		</div>
	</div>
	<div style="background-color: rgb(0, 26, 31);width: 100%;height: 890px;padding: 10px 0px 0px 10px;text-indent: 50px;">
		<div class="content">
            <h1>Edit splash:</h1>
            <h3>Title</h3>
            <form method = "POST" name="editSplash" action="updateSplash.php">
                <input type="text" name="splashTitle" size="100"></input>
                <h3>Body</h3>
                <p><input type="text" name="splashText" size ="100"></input></p>
                <p><input type="submit" value="Submit" name="submitSplash">
				<input type="reset" value="Reset"></p>
            </form>
            <h1>Post new article</h1>
            <h3>Title</h3>
            <form method = "POST" name="newArticle" action="newarticle.php">
                <input type="text" name="articleTitle" size="100"></input>
                <h3>Body</h3>
                <p><textarea name="articleText" rows="20" cols="100"></textarea></p>
                <p><input type="submit" value="Submit">
				<input type="reset" value="Reset"></p>
                <br />
                <a style="color:white" href = "logout.php">Logout</a>
            </form>
        </div>
	</div>
	<div class="footer">
		<div class="footerleft">
			<p>You are currently logged in.</p>
			<p>This is a made up organization. None of the content on this page is real.</p>
		</div>
		<div class="footerright">
			<p><b>Daytona Beach Youth Theatre</b></p>
			<p>500 Avenue Ave.</p>
			<p>Daytona Beach, Florida 32334</p>
			<p>555-555-5555</p>
		</div>
	</div>
</body>
</html>