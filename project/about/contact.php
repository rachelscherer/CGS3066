<!DOCTYPE html>
<html>
<head>
	<title>Contact us</title>
	<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
	<?php
		include '../mysql_info.php'; 
    ?>
    <div class="top">
	<div class="topbar">
		<div class="topcontentpad">
        <a href="../index.php"><img src="../logo.png" alt="Working logo" /></a>
		<div class="dropdown">
			<button class="dropbtn">Become Involved</button>
			<div class="dropdown-content">
				<a href="../becomeinvolved/volunteer.php">Volunteer</a> <a href ="../becomeinvolved/member.php">Become a Member</a><a href="../becomeinvolved/auditions.php">Audition</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">Shows</button>
			<div class="dropdown-content">
				<a href="../shows/current.php">Current Season</a> <hr /> <a href="../shows/0708.php">2007-2008 Season</a> <a href="../shows/0809.php">2008-2009 Season</a> <a href="../shows/0910.php">2009-2010 Season</a> <a href="../shows/1011.php">2010-2011 Season</a> <a href="../shows/1112.php">2011-2012 Season</a> <a href="../shows/1213.php">2012-2013 Season</a> <a href="../shows/1314.php">2013-2014 Season</a> <a href="../shows/1415.php">2014-2015 Season</a> <a href="../shows/1516.php">2015-2016 Season</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">News</button>
			<div class="dropdown-content">
				<a href="../news/articles.php">Articles</a> <a href="../news/email.php">Email List</a>
			</div>
		</div>
		<div class="dropdown">
			<button class="dropbtn">About Us</button>
			<div class="dropdown-content">
				<a href="../about/mission.php">Mission</a> <a href="../about/history.php">History</a> <a href="../about/staff.php">Staff</a> <a href ="#">Contact Us</a>
			</div>
		</div>
		<div class="dropdown">
				<a href="../login.php"><button class="dropbtn">Edit</button></a>
		</div>
		</div>
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<h1 class="logo">Daytona Beach Youth Theatre</h1>
	</div>
	</div>
	<div class="container1">
		<div class="content">
			<span style="text-align: center; font-size: 1.65em"><i><h1>Contact us</h1></i></span>
		</div>
	</div>
	<div class="container2show">
		<div class="content">
			<p><span style="font-size: 1.5em"><b>We'd love you hear from you!</b></span></p>
			<p>Please fill out your information below, and we'll get back to you as soon as possible!</p>
			<form method = "POST" name="myForm" onsubmit="return checkForm();" action="sendemail.php">  
				<b>Name:</b> <input type="text" name="name"> <span id="nameErr"></span> <br /><br />
				<b>E-mail:</b> <input type="text" name="email"> <span id="emailErr"></span><br /><br />
				<b>Comment:</b><br />
				<textarea name="comment" rows="10" cols="50"></textarea><br /><br />
				<input type="submit" value="Submit">
				<input type="reset" value="Reset"> <span id="submitErr" style="color:red"></span>
			</form>
		</div>
	</div>
	<div class="footer">
		<div class="footerleft">
			<?php
			if ($_SESSION['login'] == "1") {
				echo "<p>You are currently logged in.</p>";
			} else{
				echo "<p>You are currently logged out</p>";
			}
			?>
			<p>This is a made up organization. None of the content on this page is real.</p>
		</div>
		<div class="footerright">
			<p><b>Daytona Beach Youth Theatre</b></p>
			<p>500 Avenue Ave.</p>
			<p>Daytona Beach, Florida 32334</p>
			<p>555-555-5555</p>
		</div>
	</div>
	<script>
		function checkForm() {
            var name = document.forms["myForm"]["name"].value;
			var nameCheck = /^[a-zA-Z\s]*$/;
			var email = document.forms["myForm"]["email"].value;
			var emailCheck = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			var error = false;
			
			if (name === ""){
				document.getElementById("nameErr").innerHTML = " Name cannot be blank";
				error = true;
			} else if (nameCheck.test(name) === false){
				document.getElementById("nameErr").innerHTML = " Name must contain only letters and spaces.";
				error = true;
			} else{
				document.getElementById("nameErr").innerHTML = "";
			}
			
			if (email === ""){
				document.getElementById("emailErr").innerHTML = " Email cannot be blank";
				error = true;
			} else if (emailCheck.test(email) === false){
				document.getElementById("emailErr").innerHTML = " Email must follow this syntax: id@domain";
				error = true;
			} else{
				document.getElementById("emailErr").innerHTML = "";
			}
			
			if (error === true){
				document.getElementById("submitErr").innerHTML = " WARNING: FORM ERRORS";
				return false;
			} else{
				document.getElementById("submitErr").innerHTML = "";
				return true;
			}
        }
	</script>
</body>
</html>